# Generated by Django 2.0.7 on 2018-08-13 04:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_auto_20180810_1122'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='attachments',
        ),
    ]
