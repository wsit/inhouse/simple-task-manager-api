# Generated by Django 2.0.7 on 2018-07-27 10:50

import api.model_helper
from django.conf import settings
import django.contrib.postgres.fields
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.CharField(default='acd35780885c', max_length=32, primary_key=True, serialize=False)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('name', models.CharField(max_length=128)),
                ('slug', models.CharField(max_length=32, unique=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('logo', models.ImageField(blank=True, default=None, editable=False, null=True, upload_to=api.model_helper.ModelHelper.get_upload_path_thumb)),
                ('status', models.CharField(choices=[('active', 'Active'), ('inactive', 'Inactive')], default='active', max_length=8)),
                ('admin_json', django.contrib.postgres.fields.jsonb.JSONField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'organizations',
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.CharField(default='8bd4f42321d8', max_length=32, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=256)),
                ('description', models.TextField(blank=True, null=True)),
                ('logo', models.ImageField(blank=True, default=None, editable=False, null=True, upload_to=api.model_helper.ModelHelper.get_upload_path_thumb)),
                ('status', models.CharField(choices=[('active', 'Active'), ('inactive', 'Inactive'), ('archived', 'Archived')], default='active', max_length=8)),
                ('member_ids', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=32), default=[], size=None)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('manager', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('organization', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Organization')),
            ],
            options={
                'db_table': 'projects',
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.CharField(default='4004a527ba31', max_length=32, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=256)),
                ('description', models.TextField(blank=True, null=True)),
                ('attachments', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=256), default=[], size=None)),
                ('status', models.CharField(choices=[('open', 'Open'), ('closed', 'Closed'), ('disputed', 'Disputed')], default='active', max_length=8)),
                ('deadline', models.DateTimeField(blank=True, null=True)),
                ('project_json', django.contrib.postgres.fields.jsonb.JSONField()),
                ('created_by_json', django.contrib.postgres.fields.jsonb.JSONField()),
                ('assignee_json', django.contrib.postgres.fields.jsonb.JSONField()),
                ('assigned_json', django.contrib.postgres.fields.jsonb.JSONField()),
                ('task_type', models.CharField(choices=[('task', 'Task'), ('meeting', 'Meeting'), ('call', 'Call')], default='task', max_length=8)),
                ('details', models.TextField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'tasks',
            },
        ),
        migrations.CreateModel(
            name='TmsUserRole',
            fields=[
                ('id', models.CharField(default='70d8ed7b4e9c', max_length=32, primary_key=True, serialize=False)),
                ('organization', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Organization')),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Project')),
                ('role', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='auth.Permission')),
                ('task', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Task')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'tms_user_roles',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('uid', models.CharField(default='01f0f0dceffa', max_length=32, primary_key=True, serialize=False)),
                ('phone_number', models.CharField(blank=True, max_length=32, null=True)),
                ('dob', models.DateField(blank=True, null=True)),
                ('organization_ids', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=32), default=[], size=None)),
                ('project_ids', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=32), default=[], size=None)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'user_profiles',
            },
        ),
    ]
