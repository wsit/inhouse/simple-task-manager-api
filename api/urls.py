from api.default_views import index
from django.urls import path, include
from api.serializer_router import url_pattern as serializer_urls
from api.views.helper_view import HelperView
from django.views.decorators.csrf import csrf_exempt

from api.views.user_profile_view import UserProfileView

urlpatterns = [
    path('', index, name="api-index"),
    path('api/common/file-upload/', csrf_exempt(HelperView.file_upload), name="file-upload"),
    path('api/user-register/',  csrf_exempt(UserProfileView.as_view()), name="user-register"),
    path('api/', include(serializer_urls))
]
