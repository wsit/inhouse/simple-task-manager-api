from api.model_helper import ModelHelper
from django.db import models
from django.contrib.auth.models import User, Permission
from django.contrib.postgres.fields import ArrayField, JSONField


class Organization(models.Model):
    org_status = (('active', 'Active'), ('inactive', 'Inactive'))

    id = models.CharField(primary_key=True, max_length=32, default=ModelHelper.generate_guid)
    email = models.EmailField(null=True, blank=True)
    name = models.CharField(max_length=128)
    slug = models.CharField(max_length=32, unique=True)
    description = models.TextField(null=True, blank=True)
    logo = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=8, choices=org_status, default='active')
    admin_json = JSONField()
    member_ids = ArrayField(base_field=models.CharField(max_length=32), default=[])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "organizations"


class UserProfile(models.Model):
    uid = models.CharField(primary_key=True, max_length=32, default=ModelHelper.generate_guid)
    user = models.OneToOneField(to=User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=32, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    organization_ids = ArrayField(base_field=models.CharField(max_length=32), default=[])
    project_ids = ArrayField(base_field=models.CharField(max_length=32), default=[])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username

    def delete(self, using=None, keep_parents=False):
        """ User deletion will delete UserProfile too """
        return self.user.delete()

    class Meta:
        db_table = "user_profiles"


class Project(models.Model):
    project_status = (('active', 'Active'), ('inactive', 'Inactive'), ('archived', 'Archived'))

    id = models.CharField(primary_key=True, max_length=32, default=ModelHelper.generate_guid)
    name = models.CharField(max_length=256)
    description = models.TextField(null=True, blank=True)
    # logo = models.ImageField(upload_to=ModelHelper.get_upload_path_thumb, editable=False, default=None, null=True, blank=True)
    logo = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=8, choices=project_status, default='active')
    organization = models.ForeignKey(to=Organization, on_delete=models.CASCADE, null=True, blank=True)
    manager = models.ForeignKey(to=UserProfile, on_delete=models.SET_NULL, null=True, blank=True)
    member_ids = ArrayField(base_field=models.CharField(max_length=32), default=[])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "projects"


class Task(models.Model):
    from django.contrib.postgres.fields.jsonb import JSONField as JSONBField
    task_status = (('open', 'Open'), ('closed', 'Closed'), ('disputed', 'Disputed'))
    task_types = (('task', 'Task'), ('meeting', 'Meeting'), ('call', 'Call'))

    id = models.CharField(primary_key=True, max_length=32, default=ModelHelper.generate_guid)
    title = models.CharField(max_length=256)
    description = models.TextField(null=True, blank=True)
    attachments = JSONBField(default=list, null=True, blank=True)
    status = models.CharField(max_length=8, choices=task_status, default='open')
    deadline = models.DateTimeField(null=True, blank=True)
    project_json = JSONField(null=True, blank=True)
    created_by_json = JSONField(null=True, blank=True)
    assignee_json = JSONField(null=True, blank=True)
    assigned_json = JSONField(null=True, blank=True)
    task_type = models.CharField(max_length=8, choices=task_types, default='task')
    details = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = "tasks"


class TmsUserRole(models.Model):
    id = models.CharField(primary_key=True, max_length=32, default=ModelHelper.generate_guid)
    user = models.ForeignKey(to=UserProfile, on_delete=models.CASCADE)
    role = models.ForeignKey(to=Permission, on_delete=models.CASCADE)
    organization = models.ForeignKey(to=Organization, on_delete=models.CASCADE, null=True, blank=True)
    project = models.ForeignKey(to=Project, on_delete=models.CASCADE, null=True, blank=True)
    task = models.ForeignKey(to=Task, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.role.name

    class Meta:
        db_table = 'tms_user_roles'
