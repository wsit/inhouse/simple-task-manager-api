from rest_framework import viewsets, status, serializers
from api.serializers.organization_serializer import OrganizationSerializer
from api.models import Organization, TmsUserRole, UserProfile
from django.contrib.auth.models import Permission
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated


class OrganizationPermission(permissions.BasePermission):
    """ Checking user's organization action permission"""

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if request.method == 'POST':
            return True
        elif request.method == 'GET':
            org_id = request.parser_context.get('kwargs', {}).get('pk')
            # org_id = None, when request is to get all organizations, (which need su permissions)
            # need to check user's organization's permission
            if org_id and TmsUserRole.objects.filter(user_id=request.user.userprofile.uid, organization_id=org_id,
                                                     role__codename='change_organization').exists():
                return True
            # return True # to get all orgs
        elif request.method in ['PUT', 'PATCH']:
            org_id = request.parser_context.get('kwargs', {}).get('pk')
            if TmsUserRole.objects.filter(user_id=request.user.userprofile.uid, organization_id=org_id,
                                          role__codename='change_organization').exists():
                return True
        elif request.method == 'DELETE':
            org_id = request.parser_context.get('kwargs', {}).get('pk')
            if TmsUserRole.objects.filter(user_id=request.user.userprofile.uid, organization_id=org_id,
                                          role__codename='delete_organization').exists():
                return True
        return False


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = (IsAuthenticated, OrganizationPermission, )

    def create(self, request, *args, **kwargs):
        uid = request.user.userprofile.uid
        admin_json = dict(
            uid=uid,
            username=request.user.username,
            first_name=request.user.first_name,
        )
        request.POST._mutable = True
        request.data['admin_json'] = admin_json
        request.POST._mutable = False

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        try:
            # start adding permission
            edit_permission = Permission.objects.get(codename='change_organization')
            delete_permission = Permission.objects.get(codename='delete_organization')
            TmsUserRole(user_id=uid, role_id=edit_permission.id, organization_id=serializer.data['id']).save()
            TmsUserRole(user_id=uid, role_id=delete_permission.id, organization_id=serializer.data['id']).save()
            # end permission
        except Exception as exception:
            print(exception)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        # Add or update user to organization
        if 'member_ids' in request.data:
            if instance.member_ids:
                member_list = UserProfile.objects.filter(uid__in=request.data['member_ids'])
                if member_list.exists():
                    all_members = list(set().union(instance.member_ids, request.data['member_ids']))
                    request.POST._mutable = True
                    request.data['member_ids'] = all_members
                    request.POST._mutable = False
                    for member in member_list:
                        if instance.id not in member.organization_ids:
                            member.organization_ids.append(instance.id)
                            member.save()
                else:
                    raise serializers.ValidationError({'member_ids':'Something went wrong. please try again'})
            else:
                member_list = UserProfile.objects.filter(uid__in=request.data['member_ids'])
                for member in member_list:
                    if instance.id not in member.organization_ids:
                        member.organization_ids.append(instance.id)
                        member.save()
        # Remove user from Organization
        if 'remove_users' in request.data:
            request.POST._mutable = True
            request.data['member_ids'] = list(
                set(instance.member_ids).difference(set(request.data['remove_users'])))
            request.POST._mutable = False
            member_list = UserProfile.objects.filter(uid__in=request.data['remove_users'])
            for member in member_list:
                member.organization_ids = list(set(member.organization_ids).difference(set([instance.id])))
                member.save()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)

