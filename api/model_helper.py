import os
import uuid
import datetime


class ModelHelper:
    """
        this class contains model's helper functions.
    """

    @staticmethod
    def generate_guid():
        return uuid.uuid4().hex[:16]

    def get_upload_path_thumb(instance, filename):
        """ creates unique-Path & filename for upload """
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (instance.image.name, ext)
        d = datetime.date.today()
        return os.path.join(
            'pictures/events/thumbs', d.strftime('%Y'), d.strftime('%m'), filename
        )
