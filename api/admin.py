from django.contrib import admin
from api.models import Organization, Project, UserProfile, Task


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    fields = ('name', 'slug', 'admin_json', )
    list_display = ('name', 'slug',)


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    fields = ('name', 'description', 'manager', )
    list_display = ('name', 'manager',)


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    fields = ('user', 'phone_number', 'dob',)
    list_display = ('user','uid',)


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    fields = ('title', 'description', 'status', )
    list_display = ('title', 'status',)
