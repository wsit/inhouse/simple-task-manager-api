from rest_framework.routers import SimpleRouter
from api.views.organization_view import OrganizationViewSet
from api.views.user_profile_view import UserProfileViewSet
from api.views.project_view import ProjectViewSet
from api.views.task_view import TaskViewSet

router = SimpleRouter()

router.register('organizations', OrganizationViewSet)
router.register('users', UserProfileViewSet)
router.register(r'^(?P<org_slug>[\w-]+)/projects', ProjectViewSet)
router.register(r'^(?P<org_slug>[\w-]+)/projects/(?P<project_id>[\w-]+)/tasks', TaskViewSet)


url_pattern = router.urls
