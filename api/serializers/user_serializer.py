from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=True)
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True, write_only=True)
    confirm_password = serializers.CharField(write_only=True)
    # email = serializers.CharField(required=False)

    # def validate(self, attrs):
    #     if attrs['password'] != attrs['confirm_password']:
    #         raise serializers.ValidationError({"password": "Did not match"})
    #     if len(attrs['password']) < 8:
    #         raise serializers.ValidationError({"password": ["Password must be at least 8 characters", "sdfsdf"]})
    #     return attrs

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password', 'confirm_password')
