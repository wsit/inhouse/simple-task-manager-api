from rest_framework import serializers
from api.models import Project


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'name', 'description', 'organization', 'manager', 'member_ids', 'logo', 'status','created_at', 'updated_at')
